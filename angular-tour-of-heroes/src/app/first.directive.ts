import { Directive, Renderer2, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[first]'
})
export class FirstDirective {

  
  private hostBind:HostBinding;

  private hostListener: HostListener;

  constructor(private elem:ElementRef,private render:Renderer2) { 
    console.log(elem);
    render.setStyle(elem.nativeElement,"width","50px"); 
  }

}
