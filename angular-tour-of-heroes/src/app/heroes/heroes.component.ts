import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  


  public selectedHero:Hero;
  public heroes:Hero[];
  public heroservice:HeroService;

  constructor(_heroservice:HeroService) {
    this.heroservice=_heroservice;
    
   }

  ngOnInit() {
    this.heroservice.getListHero().subscribe(
      heroes => this.heroes = heroes
      )   
  }





public getListHero():Hero[]{
  return this.heroes;
}


  public selectHero(hero:Hero):void{
    this.selectedHero=hero;
    console.log(hero);
  }




}
