import { Injectable, EventEmitter } from '@angular/core';
import { Hero } from './hero';
import { of, Observable, interval } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root' 
})
export class HeroService {

  private heroesUrl = 'api/heroes';  // URL to web api

  private eventEmmiter:EventEmitter<string> = new EventEmitter<string>();


  private messageService:MessageService;

  constructor(_messageService:MessageService, private http: HttpClient) { 
    console.log("<<<<<HeroService>>>>>");
    this.messageService=_messageService;


   
    
    /*
    
    //Um observable rodando em interval de 1 seg.
    const secondsCounter = interval(1000);
    // Subscribe to begin publishing values
    secondsCounter.subscribe(n =>
      console.log(`It's been ${n} seconds since subscribing!`));
      

      interval(1000).subscribe(n =>
        console.log(`It's been ${n} seconds since subscribing!`));
        
        

      //UM MAP!
      const map = new Map();
      map.set(1, 'Hi');
      map.set(2, 'Bye');

      
      console.log(map.get(1));
      */
      
  }

 

  public getListHero(): Observable<Hero[]>{
    this.messageService.add('HeroService: fetched heroes');
    this.eventEmmiter.emit("getListHero");

    return this.http.get<Hero[]>(this.heroesUrl);
    
        
    //return of(this.HEROES);
  }




/*
import { HttpClient, HttpHeaders } from '@angular/common/http';
  private http: HttpClient,

getUsers(): Observable<User[]> {
    return this.http.get(myApiUrl)
                    .map(res=>res.json())
                    .catch(err=> Observable.throw(err.message));
 } 

 --angular site
getHeroes (): Observable<Hero[]> {
  return this.http.get<Hero[]>(this.heroesUrl)
    .pipe(
      catchError(this.handleError('getHeroes', []))
    );
}


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

updateHero (hero: Hero): Observable<any> {
  return this.http.put(this.heroesUrl, hero, httpOptions).pipe(
    tap(_ => this.log(`updated hero id=${hero.id}`)),
    catchError(this.handleError<any>('updateHero'))
  );
}

 */


  private  HEROES: Hero[] = [
    new Hero(11,'Mr. Nice'),
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' },
    { id: 20, name: 'Tornado' }
  ];
}